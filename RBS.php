<?php
	require_once './includes/DbKonektor.php';

	if( isset($_GET['getListaRBS']) ) {
		//konekcija na bazu
		$konektor = new DbKonektor();

		$rbsData = new XMLWriter();
		$rbsData->openMemory();
		$rbsData->startElement("getListaRBS");

		//podaci o rbsima
		$upit = "SELECT * FROM radio_bazne_stanice";
		$rezultat = $konektor->upit($upit);

		if (is_object($rezultat))
		{
			//greška prilikom čitanja podataka
			$rbsData->startElement("greska");
				$rbsData->writeElement("uspesno", "true");
				$rbsData->writeElement("poruka", "Poruka o grešci");
			$rbsData->endElement();

			//povratni XML string sa podacima o rbsima
			$rbsData->startElement("lista");

			//imena polja u tabeli baze su identična imenima polja tabele ekrana
			while( $row = $konektor->fetchObject($rezultat) ){
				$rbsData->startElement("rbs");
					$rbsData->writeElement("id_rbs", ($row->id_rbs)."");
					$rbsData->writeElement("id_operatera", ($row->id_operatera)."");
					$rbsData->writeElement("naziv_rbs", ($row->naziv_rbs)."");
					$rbsData->writeElement("adresa_rbs", ($row->adresa_rbs)."");
					$rbsData->writeElement("oznaka_grada", ($row->adresa_rbs)."");
				$rbsData->endElement();
			}

			$rbsData->endElement();
		}
		else
		{
			//greška prilikom čitanja podataka
			$rbsData->startElement("greska");
				$rbsData->writeElement("uspesno", "false");
				$rbsData->writeElement("poruka", "Neuspešno izvršenje naredbe SELECT");
			$rbsData->endElement();

			//povratni XML string sa podacima o komentarima
			$rbsData->startElement("lista");
			$rbsData->endElement();
		}

		$rbsData->endElement();

		//oslobađanje resursa i zatvaranje konekcije
		$konektor->oslobodiResurse($rezultat);
		$konektor->zatvori();

		$returnXML = $rbsData->outputMemory();
		echo $returnXML;
	}

	if( isset($_GET['getListaOS']) ) {
		//konekcija na bazu
		$konektor = new DbKonektor();

		$rbsData = new XMLWriter();
		$rbsData->openMemory();
		$rbsData->startElement("getListaOS");

		//podaci o rbsima
		$upit = "SELECT * FROM odgovorna_osoba";
		$rezultat = $konektor->upit($upit);

		if (is_object($rezultat))
		{
			//greška prilikom čitanja podataka
			$rbsData->startElement("greska");
				$rbsData->writeElement("uspesno", "true");
				$rbsData->writeElement("poruka", "Poruka o grešci");
			$rbsData->endElement();

			//povratni XML string sa podacima o rbsima
			$rbsData->startElement("lista");

			//imena polja u tabeli baze su identična imenima polja tabele ekrana
			while( $row = $konektor->fetchObject($rezultat) ){
				$rbsData->startElement("odgovorna_osoba");
					$rbsData->writeElement("id_odgovorna_osoba", ($row->id_odgovorna_osoba)."");
					$rbsData->writeElement("ime", ($row->ime)."");
					$rbsData->writeElement("prezime", ($row->prezime)."");
					$rbsData->writeElement("radno_mesto", ($row->radno_mesto)."");
					$rbsData->writeElement("telefon_1", ($row->telefon_1)."");
					$rbsData->writeElement("email_1", ($row->email_1)."");
				$rbsData->endElement();
			}

			$rbsData->endElement();
		}
		else
		{
			//greška prilikom čitanja podataka
			$rbsData->startElement("greska");
				$rbsData->writeElement("uspesno", "false");
				$rbsData->writeElement("poruka", "Neuspešno izvršenje naredbe SELECT");
			$rbsData->endElement();

			//povratni XML string sa podacima o komentarima
			$rbsData->startElement("lista");
			$rbsData->endElement();
		}

		$rbsData->endElement();

		//oslobađanje resursa i zatvaranje konekcije
		$konektor->oslobodiResurse($rezultat);
		$konektor->zatvori();

		$returnXML = $rbsData->outputMemory();
		echo $returnXML;
	}

	if( isset($_GET['getListaMO']) ) {
		//konekcija na bazu
		$konektor = new DbKonektor();

		$rbsData = new XMLWriter();
		$rbsData->openMemory();
		$rbsData->startElement("getListaMO");

		//podaci o rbsima
		$upit = "SELECT * FROM mobilni_operateri";
		$rezultat = $konektor->upit($upit);

		if (is_object($rezultat))
		{
			//greška prilikom čitanja podataka
			$rbsData->startElement("greska");
				$rbsData->writeElement("uspesno", "true");
				$rbsData->writeElement("poruka", "Poruka o grešci");
			$rbsData->endElement();

			//povratni XML string sa podacima o rbsima
			$rbsData->startElement("lista");

			//imena polja u tabeli baze su identična imenima polja tabele ekrana
			while( $row = $konektor->fetchObject($rezultat) ){
				$rbsData->startElement("mobilni_operater");
					$rbsData->writeElement("id_operatera", ($row->id_operatera)."");
					$rbsData->writeElement("naziv_operatera", ($row->naziv_operatera)."");
					$rbsData->writeElement("adresa", ($row->adresa)."");
					$rbsData->writeElement("grad", ($row->grad)."");
					$rbsData->writeElement("pib", ($row->pib)."");
					$rbsData->writeElement("broj_racuna", ($row->broj_racuna)."");
				$rbsData->endElement();
			}

			$rbsData->endElement();
		}
		else
		{
			//greška prilikom čitanja podataka
			$rbsData->startElement("greska");
				$rbsData->writeElement("uspesno", "false");
				$rbsData->writeElement("poruka", "Neuspešno izvršenje naredbe SELECT");
			$rbsData->endElement();

			//povratni XML string sa podacima o komentarima
			$rbsData->startElement("lista");
			$rbsData->endElement();
		}

		$rbsData->endElement();

		//oslobađanje resursa i zatvaranje konekcije
		$konektor->oslobodiResurse($rezultat);
		$konektor->zatvori();

		$returnXML = $rbsData->outputMemory();
		echo $returnXML;
	}

	if( isset($_POST['insertRBS']) ) {
		//konekcija na bazu
		$konektor = new DbKonektor();

		$rbs = simplexml_load_string( $_POST['rbsData'] );

		//dodavanje podataka u tabelu 'rbs'
		$upit = "INSERT INTO radio_bazne_stanice (id_operatera, naziv_rbs, adresa_rbs, oznaka_grada) VALUES
					('$rbs->id_operatera', '$rbs->naziv_rbs', '$rbs->adresa_rbs', '$rbs->oznaka_grada')";


		if( $konektor->upit($upit) )
			$greska = "<greska>
						 <uspesno>true</uspesno>
						 <poruka>Poruka o grešci</poruka>
					   </greska>";
		else
			//greška prilikom snimanja podataka
			$greska = "<greska>
						 <uspesno>false</uspesno>
						 <poruka>Neuspešno izvršenje naredbe INSERT</poruka>
					   </greska>";

		//zatvaranje konekcije
		$konektor->zatvori();

		echo $greska;
	}

		if( isset($_POST['updateRBS']) ) {
		//konekcija na bazu
		$konektor = new DbKonektor();

		$rbs = simplexml_load_string( $_POST['rbsData'] );

		//ažuriranje podataka u tabeli 'rbs'
		$upit = "UPDATE radio_bazne_stanice SET id_operatera = '$rbs->id_operatera', naziv_rbs = '$rbs->naziv_rbs', adresa_rbs = '$rbs->adresa_rbs', oznaka_grada = '$rbs->oznaka_grada'
					 WHERE id_rbs = '$rbs->id_rbs'";

		if( $konektor->upit($upit) )
			$greska = "<greska>
						 <uspesno>true</uspesno>
						 <poruka>Poruka o grešci</poruka>
					   </greska>";
		else
			//greška prilikom snimanja podataka
			$greska = "<greska>
						 <uspesno>false</uspesno>
						 <poruka>Neuspešno izvršenje naredbe UPDATE</poruka>
					   </greska>";

		//zatvaranje konekcije
		$konektor->zatvori();

		echo $greska;
	}

	if(isset($_POST['deleteRBS'])) {
		//ID rbsa koji se briše
		$id_rbs = mysql_real_escape_string($_POST['id_rbs']);


		//konekcija na bazu
		$konektor = new DbKonektor();

		//brisanje podataka iz tabele 'rbs'
		$upit = "DELETE FROM radio_bazne_stanice WHERE radio_bazne_stanice.id_rbs = $id_rbs";

		if( $konektor->upit($upit) )
			$greska = "<greska>
						 <uspesno>true</uspesno>
						 <poruka>Poruka o grešci</poruka>
					   </greska>";
		else
			//greška prilikom snimanja podataka
			$greska = "<greska>
						 <uspesno>false</uspesno>
						 <poruka>Neuspešno izvršenje naredbe DELETE</poruka>
					   </greska>";

		//zatvaranje konekcije
		$konektor->zatvori();

		echo $greska;
	}

?>
